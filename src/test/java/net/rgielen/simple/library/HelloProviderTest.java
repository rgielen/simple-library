package net.rgielen.simple.library;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class HelloProviderTest {

    @Test
    public void sayHelloReturnsValidResultForGivenName() throws Exception {
        assertThat(new HelloProvider().sayHelloTo("Rene")).isEqualTo("Hello Rene");
    }

    @Test
    public void sayHelloReturnsInValidResultForGivenName() throws Exception {
        assertThat(new HelloProvider().sayHelloTo("Rene")).isNotEqualTo("Hello Heinz");
    }

}